<?php

/**
 * Spector
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license 
 * available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */

/**
 * Page callback 
 */
function spector_config_page()
{
  $output =  drupal_get_form('spector_settings_form');
    
  return $output;
}

/**
 * Settings form
 * 
 * @param array $form_state
 * @return array the form 
 */
function spector_settings_form($form_state)
{
  $form['db'] = array(
    '#type'  => 'textfield',
    '#required'  => true,
    '#title' => t('MongoDB Server'),
    '#description' => t('All mongodb:// URI schemes are supported here, including authentication. For example: mongodb://user:password@localhost:27017'),
    '#default_value' => variable_get(SPECTOR_MONGODB_URI)
  );
  
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );
  
  return $form;
}

function spector_settings_form_validate($form, &$form_state)
{
}

function spector_settings_form_submit($form, &$form_state)
{
  $values = $form_state['values'];
	variable_set(SPECTOR_MONGODB_URI, $values['db']);
	
	drupal_set_message(t('Settings have been updated.'));
}