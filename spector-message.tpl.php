<?php 

/*
 * Variables:
 *   - $entry: the entry object 
 *   - $data: assoc array with all fields safe to output
 */

?>

<div class="spector-message">
	<?php foreach ($data as $key => $value): ?>
		<div>
		  <span class="spector-field-label"><?php print $key; ?></span>
		  <span class="spector-field-value"><?php print $value; ?></span>
		</div>
	<?php endforeach; ?>
</div>