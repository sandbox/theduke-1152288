<?php

/**
 * Spector
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license 
 * available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */

/**
 * Returns HTML for a log message.
 *
 * @param $variables
 *   An associative array containing:
 *   - entry: db object
 *   - link: (optional) Format message as link
 *
 * @ingroup themeable
 */
function theme_spector_message_list($variables) {
  $output = '';
  $entry = $variables['entry'];

  $output = l($entry->message, 'spector/entry/' . $entry->_id);
  
  return $output;
}

function spector_entries_page()
{
	drupal_set_title('Spector Log Entries');
	
	drupal_add_css('.spector-th-message { width: 60%; }' ,'inline');
	
	$db = new Spector_Db();
	if (!($mongo = $db->getMongo())) return '';
	
	$table = new Spector_Table();
	
	$data = $table->extractRequestArguments();
	$filter = $data['filter'];
	
	$query = $db->buildQuery($filter);
	
	$entryCount = $db->getLogEntryCount($query);
	$pageCount = ceil($entryCount / $data['limit']);  
	$curPage = isset($_GET['page']) ? (int) $_GET['page'] : 0;
	$offset = $curPage * $data['limit'];
	
	$entries = $db->getLogEntries(
	  $query, 
	  $data['sortField'], 
	  $data['sortOrder'], 
	  $data['limit'],
	  $offset);
	
	if (!$entryCount)
	{
		$query = $_SESSION['spector_filter'] = array();
		drupal_set_message(t('No items found for your filter settings. Filter has been reset.'), 'warning');
	}
	  
	$build['spector_filter_form'] = drupal_get_form('spector_filter_form', $query);

  $header = array(
    '', // Icon column.
    array('data' => t('Severity'), 'field' => 'w.severity'),
  	array('data' => t('Project'), 'field' => 'w.project'),
  	array('data' => t('Environment'), 'field' => 'w.environment'),
  	array('data' => t('Bucket'), 'field' => 'w.bucket', 'class' => array('spector-th-bucket')),
    array('data' => t('Time'), 'field' => 'w.time', 'sort' => 'desc', 'class' => array('spector-th-time')),
    array('data' => t('Message'), 'class' => array('spector-th-message'))
  );
  
  $rows = array();
  
  $severityMap = watchdog_severity_levels();

  foreach ($entries as $entry) {
  	$entry = (object) $entry;
    
    $severity = isset($severityMap[$entry->severity]) ? $severityMap[$entry->severity] : $entry->severity;
    $time = ($entry->time instanceof MongoDate) ? $entry->time->sec : $entry->time;    
    
    $rows[] = array('data' =>
      array(
        // Cells
        array('class' => 'icon'),
        t($severity),
        $entry->project,
        $entry->environment,
        $entry->bucket,
	format_date($time, 'short'),
        theme('spector_message_list', array('entry' => $entry, 'link' => true)),
      ),
      // Attributes for tr
      'class' => ''
      //'class' => array(drupal_html_class('dblog-' . $dblog->type), $classes[$dblog->severity]),
    );
  }

  $build['spector_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'spector-logs'),
    '#empty' => t('No log messages available.'),
  );
  
  global $pager_page_array, $pager_total;
  
  $pager_page_array = array(0 => $curPage);
  $pager_total = array(0 => $pageCount);
//theme_pager();
  $build['spector_pager'] = array('#theme' => 'pager');

  return $build;
	
}

/**
 * Return form for dblog administration filters.
 *
 * @ingroup forms
 * @see spector_filter_form_submit()
 * @see spector_filter_form_validate()
 */
function spector_filter_form($form, $form_state, $query=array()) 
{
  drupal_add_css(drupal_get_path('module', 'spector') . '/spector.css');
	
  $filters = Spector_Table::getFilters($query);

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
  	'#tree' => true,
    '#collapsible' => TRUE,
    '#collapsed' => empty($_SESSION['spector_filter']),
  );
  
  foreach ($filters as $key => $filter) {
    if (!empty($_SESSION['spector_filter'][$key])) 
    {
    	$value = $_SESSION['spector_filter'][$key];
        $filter['#default_value'] = $value;
    }
    $form['filters'][$key] = $filter;
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['spector_filter'])) {
    $form['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from dblog administration filter form.
 */
function spector_filter_form_validate($form, &$form_state) 
{
}

/**
 * Process result from dblog administration filter form.
 */
function spector_filter_form_submit($form, &$form_state) 
{
	$values = $form_state['values'];
	$filterValues = $values['filters'];
  $op = $values['op'];
  $filters = Spector_Table::getFilters();
  
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($filterValues[$name])) 
        {
        	$value = $filterValues[$name];
        	if (empty($value)) continue;	
          $_SESSION['spector_filter'][$name] = $value;
        }
      }
      break;
    case t('Reset'):
      $_SESSION['spector_filter'] = array();
      break;
  }
  
  return 'spetor/entries';
}


function spector_entry_page($id)
{
	$db = new Spector_Db();
	$entry = $db->getEntry($id);
	if (!$entry)
	{
		drupal_not_found();
		return;
	}
	
	drupal_set_title('Spector Entry ' . $entry->_id);
	
	$data = (array) $entry;
	$rows = array();
	
	foreach ($data as $key => $value)
	{
		switch ($key) {
			case 'severity':
				$value = Spector_Helper::mapSeverity($value);
				break;
			case 'data':
				$tmp = json_decode($value);
				if (!$tmp) $data = unserialize($value);
				
				$value = $tmp ? '<pre>' . print_r($tmp, true) . '</pre>' : '';
				break;
			default:
				break;
		}
		
		$rows[] = array($key, $value);
	}
	
	$build['spectormessage'] = array(
    '#theme' => 'table',
    '#header' => array('', ''),
    '#rows' => $rows,
    '#attributes' => array('id' => 'spector-logs'),
    '#empty' => t('No data.'),
  );

  return $build;
}
