<?php

/**
 * Spector
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license 
 * available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */

class Spector_Table
{
	public static function getFilters($query=array())
	{
		$db = new Spector_Db();
		$filters = array();
		
		$projects = $db->getProjects($query);
		
		if (count($projects))
		{
			$filters['project'] = array(
	      '#title' => t('Project'),
	      '#type' => 'select',
	      '#multiple' => true,
	      '#size' => 8,
	      '#options' => array_combine($projects, $projects)
	    );
		}
		
			
	  $filters['severity'] = array(
	    '#title' => t('Severity'),
	    '#type' => 'select',
	    '#multiple' => true,
	    '#size' => 8,
	    '#options' => watchdog_severity_levels()
	  );
		
		if (!isset($query['project'])) return $filters;
		
		$buckets = $db->getBuckets($query);
		
		if (count($buckets))
		{
			$filters['bucket'] = array(
	      '#title' => t('Buckets'),
	      '#type' => 'select',
	      '#multiple' => true,
	      '#size' => 8,
	      '#options' => array_combine($buckets, $buckets)
	    );
		}
		
		$environments = $db->getEnvironments($query);
		
		if (count($environments))
		{
			$filters['environment'] = array(
	      '#title' => t('Environments'),
	      '#type' => 'select',
	      '#multiple' => true,
	      '#size' => 8,
	      '#options' => array_combine($environments, $environments)
	    );
		}
		
		$types = $db->getTypes($query);
		
		if (count($types))
		{
			$filters['type'] = array(
	      '#title' => t('Type'),
	      '#type' => 'select',
	      '#multiple' => true,
	      '#size' => 8,
	      '#options' => array_combine($types, $types)
	    );
		}
		
		return $filters;
	}
	
	public function extractRequestArguments()
	{
		$data = array(
			'filter' => array(),
			'sortField' => '',
			'sortOrder' => -1,
			'limit' => 20
		);
		
		$filter = ($_SESSION && isset($_SESSION['spector_filter'])) ?  $_SESSION['spector_filter'] : array();
		$g = $_GET;
		
		$data['filter'] = $filter;
		
		if (isset($g['order'])) $data['sortField'] = strtolower($g['order']);
		if (isset($g['sort']))
		{
			$data['sortOrder'] = ($g['sort'] === 'asc') ? 1 : -1;
		}
		if (isset($filter['limit'])) $data['limit'] = $filter['limit'];
				
		return $data;
	}
}