<?php

/**
 * Spector
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license 
 * available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */

class Spector_Db
{
	/**
	 * @var Mongo
	 */
	protected static $_mongo;
	
	public function __construct()
	{
		if (!self::$_mongo) $this->getMongo();
	}
	
	public function getMongo()
	{
		if (self::$_mongo) return self::$_mongo;
		
		if (!($db = variable_get(SPECTOR_MONGODB_URI))) {
			drupal_set_message(t('Spector DB is not defined. Go to admin/settings/spector.'), 'error');
			return null;
		}
		try {
			$mongo = new Mongo($db);
		} catch (Exception $e) {
			drupal_set_message(t('Could not connect to MongoDB with uri %uri', array('%uri' => $db)), 'error');
			return null;
		}
		
		self::$_mongo = $mongo;
		return $mongo;
	}
	
	public function getEntry($id)
	{
		$entry = self::$_mongo->spector->log_entries->findOne(array('_id' => new MongoId($id)));
		
		return $entry ? (object) $entry : null;
	}
	
	public function getLogEntries(array $query=array(), $sortField=null, $sortOrder=-1, $limit=20, $offset=0)
	{
		$cursor = self::$_mongo->spector->log_entries->find($query);
		if ($sortField) $cursor = $cursor->sort(array($sortField => $sortOrder));
		if ($limit) $cursor = $cursor->limit($limit);
		if ($offset) $cursor = $cursor->skip($offset);
		
		return $cursor;
	}
	
	public function getLogEntryCount(array $query=array())
	{
		return self::$_mongo->spector->log_entries->count($query);
	}
	
	public function buildQuery(array $filter=array())
	{
		$query = array();
		
		foreach ($filter as $key => $value)
		{
			switch ($key)
			{
				case 'project':
				case 'type':
				case 'environment':
				case 'bucket':
					
					$query[$key] = array('$in' => array_values($value));
					break;
				case 'severity':
					$vals = array();
					foreach ($value as $v) $vals[] = (int) $v;
					
					$query[$key] = array('$in' => $vals);
					break;
				default:
					throw new Exception("Unknown field $key");
			}
		}
		
		return $query;
	}
	
	public function getProjects($query=array())
	{
		return $this->distinct('spector', 'log_entries', 'project', $query);		
	}
	
	public function getBuckets($query=array())
	{
		return $this->distinct('spector', 'log_entries', 'bucket', $query);	
	}
	
	public function getEnvironments($query=array())
	{
		return $this->distinct('spector', 'log_entries', 'environment', $query);
	}
	
	public function getTypes($query=array())
	{
		return $this->distinct('spector', 'log_entries', 'type', $query);
	}
	
	public function distinct($db, $collection, $key, $query=array())
	{
		$result = self::$_mongo->selectDb($db)->command(array(
		  'distinct' => $collection, 
		  'key' => $key, 
		  'query' => $query));
		

		return $result['values'];
	}
	
}

class Spector_Pager
{
	public function fetchItems(array $filter)
	{
		
	}
}
